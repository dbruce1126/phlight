<div class="header">
  <h3 class="text-muted">{{ mainTitle|e }}</h3>
</div>
<div class="jumbotron">
  <h1>{{ 'Phlight Skeleton Framework' }} </h1>
  <p class="lead">Your phalcon is ready to fly.</p>
  <p><a class="btn btn-lg btn-success" href="#" role="button">View on github</a></p>
  <p>Phlight is a basic skeleton for starting a web application using Phalcon PHP.</p>
</div>
<div class="row">
  <div class="col-lg-12">
    <h4>Getting started</h4>
    <ol>
      <li>
        <p>Copy the sample config file:</p>
        <p>
          <code>cp app/config/config.inc.sample.php app/config/config.inc.local.php</code>
        </p>
      </li>
      <li>
        <p>Edit <code>app/config/config.inc.local.php</code> to your specific settings (e.g. database credentials, cache location, etc).</p>
      </li>
      <li>
        <p>Happy coding!</p>
      </li>
    </ol>
  </div>
</div>
