<?php

namespace App\Controllers;

abstract class BaseController extends \Phalcon\Mvc\Controller
{
    public function initialize()
    {
        $this->view->mainTitle = $this->config->mainTitle;
        $this->initAssets();
    }

    protected function initAssets()
    {
        $this->initCss();
        $this->initJs();
    }

    protected function initCss()
    {
        $this->assets->collection(
            'header'
        )->addCss(
            '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css',
            false
        )->addCss(
            'css/jumbotron-narrow.css'
        );
    }

    protected function initJs()
    {
        $this->assets->collection(
            'footer'
        )->addJs(
            '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js',
            false
        )->addJs(
            '//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
            false
        );
    }
}
