<?php

namespace App\Bootstrap;

class DefaultBootstrap
{
    private static $configPath = array(
        'app', 'config', 'config.inc.php'
    );

    protected $projectRoot;
    protected $di;

    public function __construct($projectRoot)
    {
        $this->projectRoot = $projectRoot;
    }

    public function init()
    {
        $this->di = new \Phalcon\DI\FactoryDefault();
        $this->initConfig();
        $this->initRouter();
        $this->initView();
        $this->initModels();
    }

    protected function initConfig()
    {
        $fullConfigPath = sprintf(
            '%s%s%s',
            $this->projectRoot,
            DIRECTORY_SEPARATOR,
            implode(DIRECTORY_SEPARATOR, self::$configPath)
        );

        if (!file_exists($fullConfigPath) || !is_readable($fullConfigPath)) {
            throw new \Exception('Cannot find default config at '.$fullConfigPath.'.');
        }
        $config = require $fullConfigPath;
        if (!is_array($config)) {
            throw new \Exception('Config found at '.$fullConfigPath.' must return an array.');
        }

        $this->di->set('config', new \Phalcon\Config($config));
    }

    protected function initRouter()
    {
        $this->di->set('url', function () {
            $url = new \Phalcon\Mvc\Url();
            $url->setBaseUri($this->di->get('config')->get('baseUri', '/'));
            return $url;
        });

        $this->di->set('router', function () {
            $router = new \Phalcon\Mvc\Router();
            $router->add(
                '/:controller/:action/:param',
                array(
                    'controller' => 1,
                    'action' => 2,
                    'params' => 3
                )
            );
            return $router;
        });

        $this->di->set('dispatcher', function () {
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
            $dispatcher->setDefaultNamespace('App\Controllers');
            return $dispatcher;
        });
    }

    protected function initView()
    {
        $this->di->set('voltService', function ($view, $di) {
            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
            $volt->setOptions(array(
                'compiledPath' => '/tmp/',
                'compiledExtension' => '.compiled',
                'stat' => $this->di->get('config')->development,
                'compileAlways' => $this->di->get('config')->development
            ));
            return $volt;
        });

        $this->di->set('view', function () {
            $view = new \Phalcon\Mvc\View();
            $viewFolder = implode(DIRECTORY_SEPARATOR, array(
                'app', 'views'
            ));
            $view->setViewsDir($this->projectRoot.DIRECTORY_SEPARATOR.$viewFolder);
            $view->registerEngines(array(
                '.volt' => 'voltService'
            ));
            return $view;
        });
    }

    protected function initModels()
    {
        $this->di->set('db', function () {
            return new \Phalcon\Db\Adapter\Pdo\Mysql(
                $this->di->get('config')->database
            );
        });
    }

    public function handleRequest()
    {
        $application = new \Phalcon\Mvc\Application($this->di);
        echo $application->handle()->getContent();
    }
}
