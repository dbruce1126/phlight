<?php

return array(
    'baseUri'  => '/',
    'development' => true,
    'mainTitle' => 'Phlight - Your phalcon is ready to fly',
    'database' => array(
        'host' => 'localhost',
        'username' => 'user',
        'password' => 'pass',
        'dbname' => 'database'
    )
);
