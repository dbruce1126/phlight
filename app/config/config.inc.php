<?php

$localConfig = array();
$localConfigPath = __DIR__.DIRECTORY_SEPARATOR.'config.inc.local.php';
if (file_exists($localConfigPath) && is_readable($localConfigPath)) {
    $localConfig = require $localConfigPath;
}

$config = array(
    'baseUri'  => '/',
    'development' => true,
    'mainTitle' => 'Phlight',
    'database' => array(
        'host' => 'localhost',
        'username' => 'user',
        'password' => 'pass',
        'dbname' => 'database'
    )
);

return array_replace_recursive($config, $localConfig);
