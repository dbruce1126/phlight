<?php

// find the path to the project root
$projectRoot = realpath(implode(DIRECTORY_SEPARATOR, array(
    __DIR__, '..'
)));
$autoloadPath = implode(DIRECTORY_SEPARATOR, array(
    'vendor', 'autoload.php'
));
require_once $projectRoot.DIRECTORY_SEPARATOR.$autoloadPath;

// bootstrap and run the project
$bootstrap = new \App\Bootstrap\DefaultBootstrap($projectRoot);
try {
    $bootstrap->init();
} catch (\Exception $e) {
    echo '<pre>Exception occurred during initialization: ';
    do {
        echo $e->getMessage().PHP_EOL.PHP_EOL;
        echo 'Stack trace:'.PHP_EOL;
        echo $e->getTraceAsString().PHP_EOL;
    } while (null !== ($e = $e->getPrevious()));
    echo '</pre>';
    exit(1);
}
$bootstrap->handleRequest();
