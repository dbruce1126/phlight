Phlight
=======

Phlight is a skeleton application for building web applications on [Phalcon PHP framework](http://phalconphp.com/). Phlight builds upon the awesomeness of Phalcon by providing an easier entry point into building a full-powered application.

Features
--------

1. Support for [Composer](https://getcomposer.org/).
2. Intuitive MVC routing and base classes for extending.
3. Namespace and autoload out of the box.
4. [Volt](http://docs.phalconphp.com/en/latest/reference/volt.html) support out of the box.
5. Support for PHPUnit and DBUnit testing.

Getting Started
---------------

1. [Install Phalcon](http://docs.phalconphp.com/en/latest/reference/install.html).

2. Clone this project using git:

 `git clone git@github.com:danbruce/phlight.git`.

3. Copy the `config.inc.sample.php` file found in `app/config` and name the copy `config.inc.local.php`.

4. Edit `config.inc.local.php` with your application-specific settings (database credentials, caching needs, etc).

5. Install the dependencies with composer:

 `php composer.phar install`

6. That's it! You're now ready to start coding.
